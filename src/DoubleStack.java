import java.util.LinkedList;

public class DoubleStack {

	private LinkedList<Double> _magazine;

	public static void main(String[] argum) {
		
	}

	DoubleStack() {
		this._magazine = new LinkedList<Double>();
	}

	@Override
	public Object clone()  throws CloneNotSupportedException{
		DoubleStack newStack = new DoubleStack();
		for(int i = _magazine.size(); i > 0; i--){
			newStack.push(_magazine.get(i-1));
		}
		return newStack;
	}

	public boolean stEmpty() {
		if (this._magazine.size() == 0) {
			return true;
		}
		return false; // TODO!!! Your code here!
	}

	public void push(double a) {
		this._magazine.addFirst(a);
	}

	public double pop() {
		if (this.stEmpty()) {
			throw new RuntimeException("magasiin on t�hi");
		}
		return this._magazine.pollFirst();
	} // pop

	public void op(String s) {
		//+ - * /
		double second = this._magazine.pop();
		double first = this._magazine.pop();
		if(s.equals("+")) {
			first = first + second;
		}
		else if(s.equals("-")) {
			first = first - second;
		}
		else if(s.equals("*")) {
			first = first * second;
		}
		else if(s.equals("/")) {
			first = first / second;
		}
		else {
			this.push(second);
		}
		this.push(first);
	}

	public double tos() {
		return this._magazine.peek(); // TODO!!! Your code here!
	}

	@Override
	public boolean equals(Object o) {
		if(o.getClass()!=this.getClass()) {
			//System.out.println("Klassi erinevus");
			return false;
		}
		else {
			DoubleStack otherOne = (DoubleStack) o;
			Object[] first = this.toArray();
			Object[] second = otherOne.toArray();
			if (first.length!=second.length) {
				//System.out.println("pikkuse erinevus");
				//System.out.println(first.length);
				//System.out.println(second.length);
				return false;
			}
			for (int i = 0; i < first.length; i++) {
				if (!first[i].equals(second[i])) {
					System.out.println("element "+i+" erinevus");
					return false;
				}
			}
			return true;
		}
	}

	@Override
	public String toString() {
		Object[] esimene = this.toArray();
		String result ="";
		for (int i = esimene.length; i > 0; i--) {
			if(i==1) {
				result=result+esimene[i-1];
			}
			else {
				result=result+esimene[i-1]+", ";
			}
			
		}
		return result; // TODO!!! Your code here!
	}

	public static double interpret(String pol) {
		//2. 15. -
		String[] calculation = pol.split(" ");
		DoubleStack numbers = new DoubleStack();
		Double number = 0.;
		for (int i = 0; i<calculation.length; i++) {
			try {
				number = Double.parseDouble(calculation[i]);
				numbers.push(number);
			} catch (NumberFormatException e) {
				if(calculation[i].equals("+") || calculation[i].equals("-") || calculation[i].equals("*") || calculation[i].equals("/")) {
					numbers.op(calculation[i]);
				}
				/*else {
					throw new RuntimeException("tundmatu m�rk");
				}*/
			}
		}
		number = numbers.pop();
		if (!numbers.stEmpty()) {
			throw new RuntimeException("magasiin �leliigseid elemente");
		}
		return number; // TODO!!! Your code here!
	}
	
	public Object[] toArray() {
		return this._magazine.toArray();
	}

}
